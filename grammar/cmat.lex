%option stack
%option noyywrap
%option never-interactive
%option nounistd
/*
 *  The scanner definition for CMat.
 */

/*
 *  Stuff enclosed in %{ %} in the first section is copied verbatim to the
 *  output, so headers and global definitions are placed here to be visible
 * to the code in the file.  Don't remove anything that was here initially
 */
%{
#include "cmat.tab.h"
#include "symboltable.h"

/* The compiler assumes these identifiers. */
#define YY_NO_UNISTD_H

/* Max size of string constants */
#define MAX_DIGITS   20
#define MAX_STR_CONST 1025
#define YY_NO_UNPUT   /* keep g++ happy */

extern FILE *fin; /* we read from this file */

/* define YY_INPUT so we read from the FILE fin:
 * This change makes it possible to use this scanner in
 * the compiler.
 */
#undef YY_INPUT
#define YY_INPUT(buf,result,max_size) \
	if ( (result = fread( (char*)buf, sizeof(char), max_size, fin)) < 0) \
		YY_FATAL_ERROR( "read() in flex scanner failed");
#define YY_USER_ACTION {yylloc.first_line = curr_lineno; yylloc.first_column = 0; yylloc.last_column=0; yylloc.last_line = curr_lineno;}

char string_buf[MAX_STR_CONST]; /* to assemble string constants */
char *string_buf_ptr;
extern YYSTYPE yylval;
extern int curr_lineno;

/*
 *  Add Your own definitions here
 */
void append(char);
%}

/*
 * Define names for regular expressions here.
 */

ELSE			else
IF				if
WHILE			while
FOR				for
RETURN			return
TYPEID 		 	int\[\]|double\[\]|int|double|string|bool|void

INT_CONST		[0-9]+
FP_CONST		[0-9]*\.[0-9]+
BOOL_CONST		true|false	

IDENTIFIER		[A-Za-z][A-Za-z0-9_]*

SINGLE_EXP		[+^\-*/=<,;{}()\[\]]
LE				<=
EQ				==
NQ				!=

MATRIXMUL		\.\*
MATRIXDIV		\.\/
MATRIXPOW		\.^	
WHITESPACE		[ \f\r\t\v]+

%x SL_COMMENT
%x COMMENT
%x STRING
%x STR_OVERFLOW

%%

 /*
  *  The multiple-character operators.
  */
"\n"			{ curr_lineno++; }

{ELSE}			{ return (ELSE); }
{IF}			{ return (IF); }
{WHILE}			{ return (WHILE); }
{FOR}			{ return (FOR); }
{RETURN}		{ return (RETURN); }
{TYPEID} 		{
					yylval.symbol = idtable.add_string(yytext);
					return (TYPEID);
				}

{FP_CONST}		{
					if(strlen(yytext) > MAX_DIGITS)
						return (ERROR);
					yylval.symbol = fptable.add_string(yytext);
					return (FP_CONST);		
				}

{INT_CONST}		{ 
					if(strlen(yytext) > MAX_DIGITS)
						return (ERROR);
					yylval.symbol = inttable.add_string(yytext);
					return (INT_CONST);	
				}
{BOOL_CONST}	{ 
					yylval.boolean = (yytext[0] == 't'); 
					return (BOOL_CONST);
				}

{IDENTIFIER}	{
					yylval.symbol = idtable.add_string(yytext);
					return (IDENTIFIER);
				}

{SINGLE_EXP} 	{ return (yytext[yyleng-1]); }	
{LE}			{ return (LE); }
{EQ}			{ return (EQ); }
{NQ}			{ return (NQ); }
{MATRIXMUL}		{ return (MATRIXMUL); }
{MATRIXDIV}		{ return (MATRIXDIV); }
{MATRIXPOW}		{ return (MATRIXPOW); } 

	/* String */
\"				{ 
					string_buf_ptr = string_buf;	
					BEGIN(STRING); 
				}

<STRING>"\""	{ 
					BEGIN(INITIAL);
					(*string_buf_ptr) = '\0';
					yylval.symbol = stringtable.add_string(string_buf);
					return (STR_CONST);
				}
<STRING><<EOF>> {
					yylval.error_msg = strdup("EOF in string constant");
					BEGIN(INITIAL);
					return (ERROR);
				}
<STRING>\n		{
					curr_lineno++;
					yylval.error_msg = strdup("Unterminated string constant");
					BEGIN(INITIAL);
					return (ERROR);
				}
<STRING>\\\0 |
<STRING>\0		{
					yylval.error_msg = strdup("String contains escaped null character");
					BEGIN(STR_OVERFLOW);
				}
<STRING>\\n	append('\n');
<STRING>\\t append('\t');
<STRING>\\f append('\f');
<STRING>\\b append('\b');	

<STRING>\\\n 	{	
					curr_lineno++;
					append('\n');
				}
<STRING>\\.		{ 
					append(yytext[1]);
				}
<STRING>[^\\\n\0\"]+ {
						//printf("matched");
						char* yptr = yytext;
						while(*yptr)
							append(*yptr++);
					 }

<STR_OVERFLOW>[^\n"]* 	/* eat up oferflown string */ 

<STR_OVERFLOW><<EOF>> |
<STR_OVERFLOW>[\n"] {
						BEGIN(INITIAL);
						return (ERROR);
					}

	/* Single line comment */
"//"			{ BEGIN(SL_COMMENT); }
<SL_COMMENT>[^\n]*	/* eat comment */
<SL_COMMENT>\n	{ curr_lineno++; BEGIN(INITIAL); }

	/* Regular comment */

"/*"			{ yy_push_state(INITIAL); BEGIN(COMMENT); }
<COMMENT>"/*"	{ yy_push_state(COMMENT); }
<COMMENT>"*/"	{ yy_pop_state(); }
<COMMENT><<EOF>> {
					yylval.error_msg = strdup("EOF in comment");
					BEGIN(INITIAL);
					return (ERROR);
				}
<COMMENT>[^*/\n]+ // eat comment
<COMMENT>"*"	 // eat single star
<COMMENT>\n		curr_lineno++;
<COMMENT>"/"	// eat single /

"*/"			{	
					yylval.error_msg = strdup("Unmatched *)");
					return (ERROR);
				}

{WHITESPACE}	/* Ignore whitespace */

.				{ 
					yylval.error_msg = strdup(yytext);
					return(ERROR); 
				}


 /*
  *  String constants (C syntax)
  *  Escape sequence \c is accepted for all characters c. Except for 
  *  \n \t \b \f, the result is c.
  *
  */


%%

void append(char val) {
	if(string_buf_ptr >= string_buf + MAX_STR_CONST - 1) {
		yylval.error_msg = strdup("String constant too long");
		BEGIN(STR_OVERFLOW);
	}
	else {
		//printf("%c %d\n", val, val);
		(*string_buf_ptr++) = val;
	}
}
