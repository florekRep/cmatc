%{
  	#include "declarations.h"
  	#include "tree.h"
 	#include "symboltable.h"
  	#include "tokens.h"
  
  	extern char *curr_filename;
  
  	/* Locations */
  	extern int node_lineno;          
      
	#define YYLLOC_DEFAULT(Current, Rhs, N)                             \
    do                                                                  \
      if (YYID (N))                                                     \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
		  node_lineno = (Current).first_line;							\
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
		  node_lineno = (Current).first_line;							\
        }                                                               \
    while (YYID (0))
    
    void yyerror(char *s);        /*  defined below; called for each parse error */
    extern int yylex();           /*  the entry point to the lexer  */
    
    /************************************************************************/
    /*                DONT CHANGE ANYTHING IN THIS SECTION                  */
    
    Program* ast_root;	      	/* the result of the parse  */
    int omerrs = 0;               /* number of errors in lexing and parsing */
    %}
    
    /* A union of all the types that can be the result of parsing actions. */
    %union {
      bool boolean;
      Symbol symbol;
      Program *program;
	  DeclarationList *declarations;
	  FunctionDeclaration *declaration;	  
	  FormalList *formals;
	  Formal *formal;	  
      StatementBlock* statement_block;

	  StatementList *statements; 
	  Statement *statement;
      Expression *expression;
      Expressions *expressions;
	  Matrix *matrix;
      MatrixRows *matrix_rows;
	  char *error_msg;    
    }
  

    %token ELSE IF WHILE FOR RETURN 
    %token LE EQ NQ ARRAY MATRIXMUL MATRIXDIV MATRIXPOW
    %token <symbol>  STR_CONST INT_CONST FP_CONST
    %token <boolean> BOOL_CONST
    %token <symbol>  TYPEID IDENTIFIER 
    %token ERROR
    
    
    /* Declare types for the grammar's non-terminals. */
    %type <program> program
	%type <declarations> function_list
	%type <declaration> function
	%type <formals> formal_list
	%type <formal> formal
    %type <statement_block> statement_block   
	%type <statements> statement_list
	%type <statement> statement
	%type <expressions> expr_list
	%type <expressions> expr_cs_list
	%type <expression> expr
	%type <matrix> matrix
	%type <matrix_rows> row_list
	%type <expressions> row
    
    /* Precedence declarations go here. */
    %right ASSIGN
	%left NOT
	%nonassoc LE '<' EQ NQ
	%left '-' '+' 
	%left '*' '/' MATRIXMUL MATRIXDIV
	%left NEG
	%right '^' MATRIXPOW
    
    %%
    /* 
    Save the root of the abstract syntax tree in a global variable.
    */

	program : 
		function_list  
			{ ast_root = new_program($1); @$ = @1; }
		;

   	function_list : 
		function
			{ $$ = new_functions($1, NULL); }
		| function_list function
			{ $$ = new_functions($2, $1); }
		;

	function:
		TYPEID IDENTIFIER '(' formal_list ')' statement_block
			{ $$ = new_function($1, $2, $4, $6); }	
		| TYPEID IDENTIFIER '(' ')' statement_block
			{ $$ = new_function($1, $2, empty_formals(), $5); }
		;

	formal_list:
		formal
			{ $$ = new_formals($1, NULL); }
		| formal_list ',' formal
			{ $$ = new_formals($3, $1); }
		; 	
	
	formal:
		TYPEID IDENTIFIER
			{ $$ = new_formal($1, $2); }
		;

	statement_block :
		'{' statement_list '}'
			{ $$ = new_statement_block($2); }
        ;

	statement_list:
		statement 
			{ $$ = new_statements($1, NULL); }
		| statement_list statement
			{ $$ = new_statements($2, $1); }
		;
	
	statement :
        RETURN expr ';'
            { $$ = new_return($2); }
	    |IF '(' expr ')' statement_block
			{ $$ = new_if($3, $5, NULL); }
		| IF '(' expr ')' statement_block ELSE statement_block
			{ $$ = new_if($3, $5, $7); }	
		| WHILE '(' expr ')' statement_block
			{ $$ = new_while($3, $5); }
		| TYPEID IDENTIFIER ';'
			{ $$ = new_local($1, $2, NULL); }
		| TYPEID IDENTIFIER '=' expr ';'
			{ $$ = new_local($1, $2, $4); }	
		| expr ';'
			{ $$ = new_expr($1); }
		;

	expr_list:
		/*empty*/
			{ $$ = empty_expr_list(); }
		| expr_cs_list
			{ $$ = $1; }
		;

	expr_cs_list:
		expr
			{ $$ = new_expressions($1, NULL); }
		| expr_cs_list ',' expr
			{ $$ = new_expressions($3, $1); }
		;

	expr:
		IDENTIFIER '=' expr
			{ $$ = new_assign($1, $3); }	
		| '-' expr %prec NEG
			{ $$ = new_neg($2); }
		| expr '+' expr
			{ $$ = new_add($1, $3); }
		| expr '-' expr
			{ $$ = new_sub($1, $3); }
		| expr '*' expr
			{ $$ = new_mul($1, $3); }
		| expr '/' expr
			{ $$ = new_div($1, $3); }
		| expr '^' expr
			{ $$ = new_pow($1, $3); }
		| expr MATRIXMUL expr
			{ $$ = new_matrixmul($1, $3); }
		| expr MATRIXDIV expr
			{ $$ = new_matrixdiv($1, $3); }
		| expr MATRIXPOW expr
			{ $$ = new_matrixpow($1, $3); }
		| expr '<' expr
			{ $$ = new_lt($1, $3); }
		| expr LE expr
			{ $$ = new_le($1, $3); }
		| expr EQ expr
			{ $$ = new_eq($1, $3); }
		| expr NQ expr
			{ $$ = new_nq($1, $3); }
		| '(' expr ')'
			{ $$ = ($2); }
		| IDENTIFIER '(' expr_list ')'
			{ $$ = new_call($1, $3); }
		| IDENTIFIER
			{ $$ = new_identifier($1); }
		| INT_CONST
			{ $$ = new_int($1); }
		| FP_CONST
			{ $$ = new_float($1); }
		| STR_CONST
			{ $$ = new_string($1); }	
		| BOOL_CONST
			{ $$ = new_bool($1); }
		| matrix
			{ $$ = $1; }
		;

	matrix:
		'[' row_list ']'
			{ $$ = new_matrix($2); }
		;
	
	row_list:
		row 
			{ $$ = new_matrixRows($1, NULL); }
		| row_list ';' row
			{ $$ = new_matrixRows($3, $1); }
		;
	
	row:
		expr
			{ $$ = new_expressions($1, NULL); }
		| row ',' expr
			{ $$ = new_expressions($3, $1); }
		;
		
    /* end of grammar */
    %%
    
    /* This function is called automatically when Bison detects a parse error. */
    void yyerror(char *s)
    {
      extern int curr_lineno;
      
      cerr << "\"" << curr_filename << "\", line " << curr_lineno << ": " \
      << s << " at or near ";
      dump_token(cerr, -1, yychar, yylval);
      cerr << endl;
      omerrs++;
      
      if(omerrs>50) {fprintf(stdout, "More than 50 errors\n"); exit(1);}
    }
    
    
