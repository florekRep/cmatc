#ifndef SEMANTIC_ANALYZER
#define SEMANTIC_ANALYZER

#include "ISemant.h"
#include "symboltable.h"
#include <string>
#include <map>
#include <vector>

class Semant_analyzer: public ISemant {
private:
	void error(int lineno, std::string msg);
	std::map<Symbol, Symbol> variables;
	std::map<Symbol, FunctionDeclaration*> functions;
	FunctionDeclaration* currentFunction;

	std::vector<ReturnStmt*> getRet(FunctionDeclaration* fd);
	void install_visible_rt();
	void check_for_main(FunctionDeclaration* );
public:
	int errors;
	Semant_analyzer();
	void run(Program * root);

	void semant(Node *p);
	void semant(Program *p);
	void semant(FunctionDeclaration* fd);
	void semant(LocalVarDeclStmt* lv);
	void semant(ExpressionStmt *p);
	void semant(ConditionalStmt *p);
	void semant(WhileLoopStmt *p);
	void semant(ReturnStmt *p);
	void semant(StatementBlock* sb);
	void semant(Call *p);
	void semant(Assign *p);
	void semant(Neg * d);
	void semant(LECmp * d);
	void semant(NQCmp * d);
	void semant(EQCmp * d);
	void semant(LTCmp * d);

	void semant(Add* a);
    void semant(Sub * s);
    void semant(Mul * m);
    void semant(Div * d);
    void semant(Pow * d);
    void semant(MatrixMul * m);
    void semant(MatrixDiv * d);
    void semant(MatrixPow * d);

	void semant(Integer* i);
	void semant(FloatingPoint* i);
	void semant(Boolean* i);
	void semant(String* s);
	void semant(Identifier * i);
	void semant(Matrix* m);
};


#endif