/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     ELSE = 258,
     IF = 259,
     WHILE = 260,
     FOR = 261,
     RETURN = 262,
     LE = 263,
     EQ = 264,
     NQ = 265,
     ARRAY = 266,
     MATRIXMUL = 267,
     MATRIXDIV = 268,
     MATRIXPOW = 269,
     STR_CONST = 270,
     INT_CONST = 271,
     FP_CONST = 272,
     BOOL_CONST = 273,
     TYPEID = 274,
     IDENTIFIER = 275,
     ERROR = 276,
     ASSIGN = 277,
     NOT = 278,
     NEG = 279
   };
#endif
/* Tokens.  */
#define ELSE 258
#define IF 259
#define WHILE 260
#define FOR 261
#define RETURN 262
#define LE 263
#define EQ 264
#define NQ 265
#define ARRAY 266
#define MATRIXMUL 267
#define MATRIXDIV 268
#define MATRIXPOW 269
#define STR_CONST 270
#define INT_CONST 271
#define FP_CONST 272
#define BOOL_CONST 273
#define TYPEID 274
#define IDENTIFIER 275
#define ERROR 276
#define ASSIGN 277
#define NOT 278
#define NEG 279



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
#include "tree.h"
typedef union YYSTYPE
{
/* Line 2058 of yacc.c  */
#line 31 "cmat.y"

      bool boolean;
      Symbol symbol;
      Program *program;
	  DeclarationList *declarations;
	  FunctionDeclaration *declaration;	  
	  FormalList *formals;
	  Formal *formal;	  
      StatementBlock* statement_block;

	  StatementList *statements; 
	  Statement *statement;
      Expression *expression;
      Expressions *expressions;
	  Matrix *matrix;
      MatrixRows *matrix_rows;
	  char *error_msg;    
    

/* Line 2058 of yacc.c  */
#line 125 "y.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;
extern YYLTYPE yylloc;
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
