#include "sem_analyzer.h"
#include "tree.h"

#include <iostream>
using namespace std;

//types
Symbol Void;
Symbol Int;
Symbol Double;
Symbol IntM;
Symbol DoubleM;
Symbol Str;
Symbol Bool;

Semant_analyzer::Semant_analyzer() : errors(0) {
	//install types
	Void = idtable.add_string("void");
	Int = idtable.add_string("int");
	Double =	idtable.add_string("double");
	IntM = idtable.add_string("int[]");
	DoubleM = idtable.add_string("double[]");
	Str = idtable.add_string("string");
	Bool = idtable.add_string("bool");

	//instal rt
	install_visible_rt();
}

void Semant_analyzer::install_visible_rt() {
	FormalList* printIMFormals = new FormalList();
	printIMFormals->push_back(new Formal(idtable.add_string("m"), IntM));
	Symbol PrintIM = idtable.add_string("printIM");
	functions[PrintIM] = new FunctionDeclaration(Void, PrintIM, printIMFormals, NULL); 

	FormalList* printDMFormals = new FormalList();
	printDMFormals->push_back(new Formal(idtable.add_string("m"), DoubleM));
	Symbol PrintDM = idtable.add_string("printDM");
	functions[PrintDM] = new FunctionDeclaration(Void, PrintDM, printDMFormals, NULL); 
}

void Semant_analyzer::run(Program* root) {
	root->accept(this);
}
void Semant_analyzer::error(int lineno, string msg) {
	errors++;
	cerr <<"Error on line " << lineno <<" "<<  msg << endl;
}
void Semant_analyzer::semant(Node *p) {
	error(p->get_line_number(), "Semant called on unknown type");
}

void Semant_analyzer::semant(Program *p) {
	FunctionDeclaration* main = NULL;
	for(int i = 0; i < p->declarations->size(); i++) {
		FunctionDeclaration * d = (*p->declarations)[i];
		if(functions.count(d->name) > 0) {
			error(p->get_line_number(), "Function redefinition " + d->name->get_string());
		} else {
			functions[d->name] = d;
		}
		d->accept(this);
		if(d->name == idtable.add_string("main")) main = d;
	}
	check_for_main(main);
}

void Semant_analyzer::check_for_main(FunctionDeclaration * fd) {
	if(fd == NULL) {
		error(0, "No main function found");
		return;
	}
	if(fd->formals->size() != 0) {
		error(fd->get_line_number(), "main: wrong number of arguments");
	}
	if(fd->return_type != Void) {
		error(fd->get_line_number(), "main: should return void");
	}
}

vector<ReturnStmt*> Semant_analyzer::getRet(FunctionDeclaration* fd) {
	vector<ReturnStmt*> returns;
	for(int i = 0; i < fd->body->statements->size(); i++) {
		Statement* s = (*fd->body->statements)[i];
		if(typeid(*s) == typeid(ReturnStmt))
			returns.push_back((ReturnStmt*)s);
	}
	return returns;
}

void Semant_analyzer::semant(FunctionDeclaration* fd) {
	variables.clear();
	currentFunction = fd;
	for(int i = 0; i < fd->formals->size(); i++) {
		Formal * f = (*fd->formals)[i];
		if(variables.count(f->name) > 0) 
			error(fd->get_line_number(), "Formal with the same name in " + fd->name->get_string() + ", " + f->name->get_string());
		else
			variables[f->name] = f->type_decl;
	}
	for(int i = 0; i < fd->body->statements->size(); i++) {
		Statement* s = (*fd->body->statements)[i];
		s->accept(this);
	}
	Symbol declaredReturn = fd->return_type;

	vector<ReturnStmt*> retStmts = getRet(fd);
	if(retStmts.size() == 0 && ! declaredReturn->equal_string("void"))
		error(fd->get_line_number(), "Function " + fd->name->get_string() + "declared return type mismatch");

	for(int i = 0; i < retStmts.size(); i++) {
		Symbol actualReturnType = retStmts[i]->ret->type;
		if(actualReturnType != declaredReturn)
			error(fd->get_line_number(), "Function " + fd->name->get_string() + "declared return type mismatch");
	}
}

void Semant_analyzer::semant(LocalVarDeclStmt* lv) {
	if(lv->init != NULL) {
		lv->init->accept(this);
		if(lv->init->type != lv->type)
			error(lv->get_line_number(), "Variable initialization has mismatching types " + lv->name->get_string());
	}

	if(variables.count(lv->name) > 0) 
		error(lv->get_line_number(), "Variable with this name " + lv->name->get_string() + " exist in function " + currentFunction->name->get_string());
	else
		variables[lv->name] = lv->type;
}

void Semant_analyzer::semant(ExpressionStmt *p) {
	p->expr->accept(this);
}
void Semant_analyzer::semant(StatementBlock* sb) {
	for(int i = 0; i < sb->statements->size(); i++) {
		Statement* s = (*sb->statements)[i];
		s->accept(this);
	}
}

void Semant_analyzer::semant(ReturnStmt* r) {
	r->ret->accept(this);
}

void Semant_analyzer::semant(Assign *p) {
	if(variables.count(p->var) == 0) {
		error(p->get_line_number(), "Not declared variable in assignment " + p->var->get_string());
		return;
	}

	p->e->accept(this);
	Symbol varType = variables[p->var];
	if(p->e->type != varType)
		error(p->get_line_number(), "Assignment mismatch types " + p->var->get_string());
	p->type = varType;
}

void Semant_analyzer::semant(Neg * d) {
	d->e->accept(this);
	Symbol type = d->e->type;
	if(type != Int && type != Double && type != DoubleM && type != IntM) {
		error(d->get_line_number(), "Only numbers can be negated");
	}
	d->type = type;
}

void Semant_analyzer::semant(NQCmp * d) {
	d->e1->accept(this);
	d->e2->accept(this);

	if(d->e1->type != d->e2->type) {
		error(d->get_line_number(), "Comparison type mismatch");
	}

	d->type = Bool;
}
void Semant_analyzer::semant(EQCmp * d) {
	d->e1->accept(this);
	d->e2->accept(this);

	if(d->e1->type != d->e2->type) {
		error(d->get_line_number(), "Comparison type mismatch");
	}

	d->type = Bool;
}

void Semant_analyzer::semant(LECmp * d) {
	d->e1->accept(this);
	d->e2->accept(this);

	if(d->e1->type != d->e2->type) {
		error(d->get_line_number(), "Comparison type mismatch");
	}
	if(d->e1->type != Int && d->e1->type != Double) {
		error(d->get_line_number(), "Comparison only on number");
	}

	d->type = Bool;
}
void Semant_analyzer::semant(LTCmp * d) {
	d->e1->accept(this);
	d->e2->accept(this);

	if(d->e1->type != d->e2->type) {
		error(d->get_line_number(), "Comparison type mismatch");
	}
	if(d->e1->type != Int && d->e1->type != Double) {
		error(d->get_line_number(), "Comparison only on number");
	}

	d->type = Bool;
}

void Semant_analyzer::semant(Add* a) {
	a->e1->accept(this);
	a->e2->accept(this);

	if(a->e1->type != a->e2->type) {
		error(a->get_line_number(), "Addition type mismatch");
	}
	else {
		Symbol type = a->e1->type;
		if(type != Int && type != Double && type != IntM && type != DoubleM) {
			error(a->get_line_number(), "Addition can only be done on numbers / matrices");
		} 
		else {
			a->type = a->e1->type;
		}
	}
}
void Semant_analyzer::semant(Sub * s) {
	s->e1->accept(this);
	s->e2->accept(this);

	if(s->e1->type != s->e2->type) {
		error(s->get_line_number(), "Subtraction type mismatch");
	}
	else {
		Symbol type = s->e1->type;
		if(type != Int && type != Double && type != IntM && type != DoubleM) {
			error(s->get_line_number(), "Subtraction can only be done on numbers / matrices");
		} 
		else {
			s->type = s->e1->type;
		}
	}
}
void Semant_analyzer::semant(Mul * m) {
	m->e1->accept(this);
	m->e2->accept(this);

	if(m->e1->type != m->e2->type) {
		error(m->get_line_number(), "Multiplication type mismatch");
	}
	else {
		Symbol type = m->e1->type;
		if(type != Int && type != Double && type != IntM && type != DoubleM) {
			error(m->get_line_number(), "Multiplication can only be done on numbers / matrices");
		} 
		else {
			m->type = m->e1->type;
		}
	}
}
void Semant_analyzer::semant(Div * d) {
	d->e1->accept(this);
	d->e2->accept(this);

	if(d->e1->type != d->e2->type) {
		error(d->get_line_number(), "Division type mismatch");
	}
	else {
		Symbol type = d->e1->type;
		if(type != Int && type != Double) {
			error(d->get_line_number(), "Division can only be done on numbers");
		} 
		else {
			d->type = d->e1->type;
		}
	}
}

void Semant_analyzer::semant(Pow * p) {
	p->e1->accept(this);
	p->e2->accept(this);

	if(p->e1->type != Int && p->e1->type != Double && p->e2->type != Int) {
		error(p->get_line_number(), "Power only in form (int/double) ^ (int) ");
	}
	else {
		p->type = p->e1->type;
	}
}

void Semant_analyzer::semant(MatrixMul * m) {
	m->e1->accept(this);
	m->e2->accept(this);

	if((m->e1->type == IntM && m->e2->type == Int) || (m->e1->type == DoubleM && m->e2->type == Double)) {
		m->type = m->e1->type;
	} else {
		error(m->get_line_number(), ".* in form (double/int)[] .* (double/int)");
	}
}

void Semant_analyzer::semant(MatrixDiv * d) {
	d->e1->accept(this);
	d->e2->accept(this);

	if((d->e1->type == IntM && d->e2->type == Int) || (d->e1->type == DoubleM && d->e2->type == Double)) {
		d->type = d->e1->type;
	} else {
		error(d->get_line_number(), "./ in form (double/int)[] ./ (double/int)");
	}
}

void Semant_analyzer::semant(MatrixPow * p) {
	p->e1->accept(this);
	p->e2->accept(this);

	if((p->e1->type != IntM && p->e2->type != Int) || (p->e1->type != DoubleM && p->e2->type != Int)) {
		error(p->get_line_number(), ".^ in form (double/int)[] .^ (int)");
	}
	p->type = p->e1->type;
}


void Semant_analyzer::semant(ConditionalStmt *p) {
	p->pred->accept(this);
	if(p->pred->type != Bool) {
		error(p->get_line_number(), "If predicate has to evaluate to bool");
	}
	p->body->accept(this);
	if(p->false_cond != NULL)
		p->false_cond->accept(this);
}
void Semant_analyzer::semant(WhileLoopStmt *p) {
	p->pred->accept(this);
	if(p->pred->type != Bool) {
		error(p->get_line_number(), "While predicate has to evaluate to bool");
	}
	p->body->accept(this);
}
void Semant_analyzer::semant(Call *c) {
	FunctionDeclaration * fd = functions[c->name];
	if(fd == NULL) {
		error(c->get_line_number(), "Call on undeclared function");
		return;
	} else {
		if(c->actual->size() != fd->formals->size()) {
			error(c->get_line_number(), "Function " + fd->name->get_string() + " called with wrong number of arguments");
		}
		for(int i = 0; i < c->actual->size(); i++) {
			Expression* e = (*c->actual)[i];
			e->accept(this);
			if(e->type != (*fd->formals)[i]->type_decl)
				error(c->get_line_number(), "Function call " + fd->name->get_string() + " argument type error");
		}
	}
	c->type = fd->return_type;
}


//Constants
void Semant_analyzer::semant(Integer* i) {
	i->type = Int;
}
void Semant_analyzer::semant(FloatingPoint* i) {
	i->type = Double;
}
void Semant_analyzer::semant(Boolean* i) {
	i->type = Bool;
}
void Semant_analyzer::semant(String* s) {
	s->type = Str;
}
void Semant_analyzer::semant(Identifier * i) {
	if(variables.count(i->name) == 0) 
		error(i->get_line_number(), "Unknown variable " + i->name->get_string());
	i->type = variables[i->name];
}
void Semant_analyzer::semant(Matrix* m) {
	Symbol type = NULL;
	int len = -1;

	for(int i = 0; i < m->matrix->size(); i++) {
		Expressions* rows = (*m->matrix)[i];
		if(len == -1) len = rows->size();
		if(len != rows->size()) {
			error(m->get_line_number(), "Matrix, rows size doesn't match");
			break;
		}
		for(int j = 0; j < rows->size(); j++) {
			Expression * e = (*rows)[j];
			e->accept(this);
			if(type == NULL) type = e->type;

			if(e->type != Int && e->type != Double)
				error(m->get_line_number(), "Error in matrix definition, type not double or int");
			if(e->type != type)
				error(m->get_line_number(), "Error in matrix, no consistient type");
		}
	}
	m->type = type == Double ? DoubleM : IntM;
}