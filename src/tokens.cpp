#include "tokens.h"
#include "symboltable.h"
#include "cmat.tab.h"

static void print_escaped_string(ostream& str, string s) {
	for (int i = 0; i < s.size(); i++) {
		switch (s[i]) {

		case '\\' : str << "\\\\"; break;
		case '\"' : str << "\\\""; break;
		case '\n' : str << "\\n"; break;
		case '\t' : str << "\\t"; break;
		case '\b' : str << "\\b"; break;
		case '\f' : str << "\\f"; break;

		default:
		  if (isprint(s[i]))
			str << s[i];
		  else 
			str << '\\' << oct << setfill('0') << setw(3)
				<< (int) ((unsigned char) (s[i]))
				<< dec << setfill(' ');
		  break;
		}
	}
}

string token_to_string(int tok) {
	switch (tok) {
		case 0:            return("EOF");        break;
		case (ELSE):       return("ELSE");       break;
		case (IF):         return("IF");         break;
		case (WHILE):      return("WHILE");      break;
		case (FOR):        return("FOR");        break;
		case (RETURN):     return("RETURN");     break;
		case (TYPEID):     return("TYPEID");     break;
		case (INT_CONST):  return("INT_CONST");  break;
		case (FP_CONST):   return("FP_CONST");   break;
		case (STR_CONST):  return("STR_CONST");  break;
	 
		case (BOOL_CONST): return("BOOL_CONST"); break;
		case (IDENTIFIER): return("IDENTIFIER"); break;
		case (LE):		   return("LE");		 break;
		case (EQ):         return("EQ");		 break;
		case (NQ):         return("NQ");         break;
		case (ARRAY):      return("ARRAY");      break;
		case (MATRIXMUL):  return("MATRIXMUL");  break;
		case (MATRIXDIV):  return("MATRIXDIV");  break;
		case (MATRIXPOW):  return("MATRIXPOW");  break;
		case (ERROR):      return("ERROR");      break;
		case '+': return("'+'"); break;
		case '/': return("'/'"); break;
		case '-': return("'-'"); break;
		case '*': return("'*'"); break;
		case '=': return("'='"); break;
		case '<': return("'<'"); break;
		case ',': return("','"); break;
		case ';': return("';'"); break;
		case ':': return("':'"); break;
		case '(': return("'('"); break;
		case ')': return("')'"); break;
		case '{': return("'{'"); break;
		case '}': return("'}'"); break;
		case '[': return("'['"); break;
		case ']': return("']'"); break;
		default:  return("<Invalid Token>");
	}
}


void dump_token(ostream& out, int lineno, int tok, YYSTYPE yylval) {
	if(lineno != -1)
		out << "#" << lineno << " ";
	out << token_to_string(tok);
    switch (tok) {
		case (STR_CONST):
			out << " \"";
			print_escaped_string(out, yylval.symbol->get_string());
			out << "\"";
			break;
		case (FP_CONST):
		case (INT_CONST):
			out << " " << yylval.symbol;
			break;
		case (BOOL_CONST):
			out << (yylval.boolean ? " true" : " false");
			break;
		case (TYPEID):
		case (IDENTIFIER):
			out << " " << yylval.symbol;
			break;
		case (ERROR): 
			if (yylval.error_msg[0] == 0) {
			  out << " \"\\000\"";
			}
			else {
			  out << " \"";
			  print_escaped_string(out, yylval.error_msg);
			  out << "\"";
			  break;
			}
		}
    out << endl;
}

