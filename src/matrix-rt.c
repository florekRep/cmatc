#include "matrix-rt.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/// INT MATRIX IMPL 
void addIM(struct matrix_int_t* r, struct matrix_int_t a, struct matrix_int_t b) {
	if(a.cols != b.cols || a.rows != b.rows) {
		fprintf(stderr, "Matrix addition error: size mismatch");
	}
	r->cols = a.cols;
	r->rows = a.rows;
	r->data = (int*)malloc(sizeof(int)*(a.cols)*(a.rows));
	for(int i = 0; i < a.cols * a.rows; i++)
		(r->data)[i] = a.data[i] + b.data[i];
}

void subIM(struct matrix_int_t* r, struct matrix_int_t a, struct matrix_int_t b) {
	if(a.cols != b.cols || a.rows != b.rows) {
		fprintf(stderr, "Matrix subtraction error: size mismatch");
	}
	r->cols = a.cols;
	r->rows = a.rows;
	r->data = (int*)malloc(sizeof(int)*(a.cols)*(a.rows));
	for(int i = 0; i < a.cols * a.rows; i++)
		(r->data)[i] = a.data[i] - b.data[i];
}

void mulIM(struct matrix_int_t* r, struct matrix_int_t a, struct matrix_int_t b) {
	if(a.cols != b.rows) {
		fprintf(stderr, "Matrix multiplication error: size mismatch");
	}
	r->cols = b.cols;
	r->rows = a.rows;
	r->data = (int*)malloc(sizeof(int)*(r->cols)*(r->rows));
	
	for(int i = 0; i < r->cols * r->rows; i++)
		(r->data)[i] = 0;

	for(int i = 0; i < r->rows; i++) {
		for(int j = 0; j < r->cols; j++) {
			for(int k = 0; k < b.rows; k++) {
				(r->data)[j + i*r->cols] += a.data[i*a.cols + k] * b.data[k*b.cols + j];
			}
		}
	}
}

void negIM(struct matrix_int_t* r, struct matrix_int_t a) {
	r->cols = a.cols;
	r->rows = a.rows;
	r->data = (int*)malloc(sizeof(int)*(a.cols)*(a.rows));
	for(int i = 0; i < a.cols * a.rows; i++)
		(r->data)[i] = -a.data[i];
}

int eqIM(struct matrix_int_t a, struct matrix_int_t b) {
	if(a.cols != b.cols || a.rows != b.rows) {
		return 0;
	}
	for(int i = 0; i < a.cols * a.rows; i++)
		if(a.data[i] != b.data[i]) return 0;
	return 1;
}

void mmulIM(struct matrix_int_t* r, struct matrix_int_t a, int b) {
	r->cols = a.cols;
	r->rows = a.rows;
	r->data = (int*)malloc(sizeof(int)*(a.cols)*(a.rows));

	for(int i = 0; i < a.cols * a.rows; i++)
		(r->data)[i] = a.data[i] * b;
}

void mdivIM(struct matrix_int_t* r, struct matrix_int_t a, int b) {
	r->cols = a.cols;
	r->rows = a.rows;
	r->data = (int*)malloc(sizeof(int)*(a.cols)*(a.rows));

	for(int i = 0; i < a.cols * a.rows; i++)
		(r->data)[i] = a.data[i] / b;
}

void cloneIM(struct matrix_int_t* dst, struct matrix_int_t src) {
	dst->cols = src.cols;
	dst->rows = src.rows;
	dst->data = (int*)malloc(sizeof(int)*(src.cols)*(src.rows));
	memcpy(dst->data, src.data, sizeof(int)*(src.cols)*(src.rows));
}

void printIM(struct matrix_int_t m) {
	for(int i = 0; i < m.rows; i++) {
		for(int j = 0; j < m.cols; j++) {
			printf("%d ", m.data[i*m.cols + j]);
		}
		printf("\n");
	}
}


/// FP MATRIX IMPL
void addDM(struct matrix_fp_t* r, struct matrix_fp_t a, struct matrix_fp_t b) {
	if(a.cols != b.cols || a.rows != b.rows) {
		fprintf(stderr, "Matrix addition error: size mismatch");
	}
	r->cols = a.cols;
	r->rows = a.rows;
	r->data = (double*)malloc(sizeof(double)*(a.cols)*(a.rows));
	for(int i = 0; i < a.cols * a.rows; i++)
		(r->data)[i] = a.data[i] + b.data[i];
}

void subDM(struct matrix_fp_t* r, struct matrix_fp_t a, struct matrix_fp_t b) {
	if(a.cols != b.cols || a.rows != b.rows) {
		fprintf(stderr, "Matrix subtraction error: size mismatch");
	}
	r->cols = a.cols;
	r->rows = a.rows;
	r->data = (double*)malloc(sizeof(double)*(a.cols)*(a.rows));
	for(int i = 0; i < a.cols * a.rows; i++)
		(r->data)[i] = a.data[i] - b.data[i];
}

void mulDM(struct matrix_fp_t* r, struct matrix_fp_t a, struct matrix_fp_t b) {
	if(a.cols != b.rows) {
		fprintf(stderr, "Matrix multiplication error: size mismatch");
	}
	r->cols = b.cols;
	r->rows = a.rows;
	r->data = (double*)malloc(sizeof(double)*(r->cols)*(r->rows));
	
	for(int i = 0; i < r->cols * r->rows; i++)
		(r->data)[i] = 0;

	for(int i = 0; i < r->rows; i++) {
		for(int j = 0; j < r->cols; j++) {
			for(int k = 0; k < b.rows; k++) {
				(r->data)[j + i*r->cols] += a.data[i*a.cols + k] * b.data[k*b.cols + j];
			}
		}
	}
}

void negDM(struct matrix_fp_t* r, struct matrix_fp_t a) {
	r->cols = a.cols;
	r->rows = a.rows;
	r->data = (double*)malloc(sizeof(double)*(a.cols)*(a.rows));
	for(int i = 0; i < a.cols * a.rows; i++)
		(r->data)[i] = -a.data[i];
}

int eqDM(struct matrix_fp_t a, struct matrix_fp_t b) {
	if(a.cols != b.cols || a.rows != b.rows) {
		return 0;
	}
	for(int i = 0; i < a.cols * a.rows; i++)
		if(a.data[i] != b.data[i]) return 0;
	return 1;
}

void mmulDM(struct matrix_fp_t* r, struct matrix_fp_t a, double b) {
	r->cols = a.cols;
	r->rows = a.rows;
	r->data = (double*)malloc(sizeof(double)*(a.cols)*(a.rows));

	for(int i = 0; i < a.cols * a.rows; i++)
		(r->data)[i] = a.data[i] * b;
}

void mdivDM(struct matrix_fp_t* r, struct matrix_fp_t a, double b) {
	r->cols = a.cols;
	r->rows = a.rows;
	r->data = (double*)malloc(sizeof(double)*(a.cols)*(a.rows));

	for(int i = 0; i < a.cols * a.rows; i++)
		(r->data)[i] = a.data[i] / b;
}

void cloneDM(struct matrix_fp_t* dst, struct matrix_fp_t src) {
	dst->cols = src.cols;
	dst->rows = src.rows;
	dst->data = (double*)malloc(sizeof(double)*(src.cols)*(src.rows));
	memcpy(dst->data, src.data, sizeof(double)*(src.cols)*(src.rows));
}

void printDM(struct matrix_fp_t m) {
	for(int i = 0; i < m.rows; i++) {
		for(int j = 0; j < m.cols; j++) {
			printf("%.2f ", m.data[i*m.cols + j]);
		}
		printf("\n");
	}
}