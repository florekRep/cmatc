#ifndef IVISITOR_H
#define IVISITOR_H
#include "llvm/IR/IRBuilder.h"

class Node;
class Integer;
class Expression;
class FloatingPoint;
class Boolean;
class Identifier;
class Add;
class Sub;
class Mul;
class Div;
class Pow;
class MatrixMul;
class MatrixDiv;
class MatrixPow;
class FunctionDeclaration;
class Program;
class StatementBlock;
class LocalVarDeclStmt;
class Call;
class ExpressionStmt;
class ConditionalStmt;
class Assign;
class Neg;
class LECmp;
class NQCmp;
class EQCmp;
class LTCmp;
class ReturnStmt;
class WhileLoopStmt;
class Matrix;

class IVisitor {
public:
	virtual llvm::Value* visit(Program *p) = 0;
	virtual llvm::Value* visit(FunctionDeclaration* fd) = 0;
	virtual llvm::Value* visit(LocalVarDeclStmt* lv) = 0;
	virtual llvm::Value* visit(ExpressionStmt *p) = 0;
	virtual llvm::Value* visit(ConditionalStmt *p) = 0;
	virtual llvm::Value* visit(ReturnStmt *p) = 0;
	virtual llvm::Value* visit(WhileLoopStmt *p) = 0;
	virtual llvm::Value* visit(StatementBlock* sb) = 0;
	virtual llvm::Value* visit(Call *p) = 0;
	virtual llvm::Value* visit(Assign *p) = 0;
	virtual llvm::Value* visit(Neg * d) = 0;
	virtual llvm::Value* visit(LECmp * d) = 0;
	virtual llvm::Value* visit(NQCmp * d) = 0;
	virtual llvm::Value* visit(EQCmp * d) = 0;
	virtual llvm::Value* visit(LTCmp * d) = 0;

	virtual llvm::Value* visit(Add* a) = 0;
    virtual llvm::Value* visit(Sub * s) = 0;
    virtual llvm::Value* visit(Mul * m) = 0;
    virtual llvm::Value* visit(Div * d) = 0;
    virtual llvm::Value* visit(Pow * d) = 0;
    virtual llvm::Value* visit(MatrixMul * m) = 0;
    virtual llvm::Value* visit(MatrixDiv * d) = 0;
    virtual llvm::Value* visit(MatrixPow * d) = 0;

	virtual llvm::Value* visit(Integer* i) = 0;
    virtual llvm::Value* visit(Identifier* i) = 0;
    virtual llvm::Value* visit(Boolean* i) = 0;
	virtual llvm::Value* visit(FloatingPoint* i) = 0;
	virtual llvm::Value* visit(Matrix* m) = 0;

	virtual llvm::Value* visit(Node* n) = 0;
};

#endif
