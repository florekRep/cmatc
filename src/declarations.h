#ifndef DECLARATIONS_H
#define DECLARATIONS_H

#include <iostream>
using std::ostream;
using std::cout;
using std::cerr;
using std::endl;

#include <fstream>
using std::ofstream;

#include <iomanip>
using std::oct;
using std::dec;
using std::setw;
using std::setfill;

#include <string>
using std::string;

#include <list>
using std::list;

#include <vector>
using std::vector;

#endif //DECLARATIONS_H
