#ifndef CMAT_PARSE_H
#define CMAT_PARSE_H

#include "io.h"

/* a type renaming */
typedef int Boolean;
class Entry;
typedef Entry *Symbol;

/* Tokens.  */
#ifndef YYTOKENTYPE
#define YYTOKENTYPE
   enum yytokentype {
	 ELSE = 258,
	 IF = 259,
	 WHILE = 260,
	 FOR = 261,
	 RETURN = 262,
	 TYPEID = 263,
	 INT_CONST = 264,
	 FP_CONST = 265,
	 BOOL_CONST = 266,
	 STR_CONST = 267,
	 IDENTIFIER = 268,
	 LE = 269,
	 EQ = 270,
	 NQ = 271,
	 ARRAY = 272,
	 MATRIXMUL = 273,
	 MATRIXDIV = 274,
	 MATRIXPOW = 275,
	 ERROR = 280
   };
#endif

/* Tokens.  */
#define ELSE 258
#define IF 259
#define WHILE 260
#define FOR 261
#define RETURN 262
#define TYPEID 263
#define INT_CONST 264
#define FP_CONST 265
#define BOOL_CONST 266
#define STR_CONST 267
#define IDENTIFIER 268
#define LE 269
#define EQ 270
#define NQ 271
#define ARRAY 272
#define MATRIXMUL 273
#define MATRIXDIV 274
#define MATRIXPOW 275
#define ERROR 280


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
  bool boolean;
  Symbol symbol;
  char *error_msg;
} YYSTYPE;

#define YYSTYPE_IS_TRIVIAL 1
#define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE cmat_yylval;

#endif