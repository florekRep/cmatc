#ifndef ISEMANT_H
#define ISEMANT_H

class Node;
class Integer;
class Expression;
class FloatingPoint;
class Boolean;
class String;
class Identifier;
class Add;
class Sub;
class Mul;
class Div;
class Pow;
class MatrixMul;
class MatrixDiv;
class MatrixPow;
class FunctionDeclaration;
class Program;
class StatementBlock;
class LocalVarDeclStmt;
class Call;
class ExpressionStmt;
class ConditionalStmt;
class Assign;
class Neg;
class LECmp;
class NQCmp;
class EQCmp;
class LTCmp;
class ReturnStmt;
class WhileLoopStmt;
class Matrix;
class ReturnStmt;

class ISemant {
public:
	virtual void semant(Node *p) = 0;

	virtual void semant(Program *p) = 0;
	virtual void semant(FunctionDeclaration* fd) = 0;
	virtual void semant(LocalVarDeclStmt* lv) = 0;
	virtual void semant(ExpressionStmt *p) = 0;
	virtual void semant(ConditionalStmt *p) = 0;
	virtual void semant(WhileLoopStmt *p) = 0;
	virtual void semant(ReturnStmt *r) = 0;
	virtual void semant(StatementBlock* sb) = 0;
	virtual void semant(Call *p) = 0;
	virtual void semant(Assign *p) = 0;
	virtual void semant(Neg * d) = 0;
	virtual void semant(LECmp * d) = 0;
	virtual void semant(NQCmp * d) = 0;
	virtual void semant(EQCmp * d) = 0;
	virtual void semant(LTCmp * d) = 0;

	virtual void semant(Add* a) = 0;
    virtual void semant(Sub * s) = 0;
    virtual void semant(Mul * m) = 0;
    virtual void semant(Div * d) = 0;
    virtual void semant(Pow * d) = 0;
    virtual void semant(MatrixMul * m) = 0;
    virtual void semant(MatrixDiv * d) = 0;
    virtual void semant(MatrixPow * d) = 0;

	virtual void semant(Integer* i) = 0;
	virtual void semant(FloatingPoint* i) = 0;
	virtual void semant(Boolean* i) = 0;
	virtual void semant(String* s) = 0;
	virtual void semant(Identifier * i) = 0;
	virtual void semant(Matrix* m) = 0;
};


#endif