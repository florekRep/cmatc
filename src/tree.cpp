#include "tree.h"

#include "ivisitor.h"
int node_lineno = 1; //global var, will be updated by bison

Node::Node() {
	line_number = node_lineno;
}

FloatingPoint* new_float(Symbol value) {
	return new FloatingPoint(value);
}

Integer* new_int(Symbol value) {
	return new Integer(value);
}

String* new_string(Symbol value) {
	return new String(value);
}

Boolean* new_bool(bool value) {
	return new Boolean(value);
}

Identifier* new_identifier(Symbol value) {
	return new Identifier(value);
}


MatrixRows* new_matrixRows(Expressions* row, MatrixRows* old) {
	if(old == NULL)
		old = new MatrixRows();
	old->push_back(row);
	return old;
}

Matrix* new_matrix(MatrixRows* m) {
	return new Matrix(m);
}

Add* new_add(Expression* e1, Expression* e2) {
	return new Add(e1,e2);
}
Sub* new_sub(Expression* e1, Expression* e2) {
	return new Sub(e1,e2);
}
Mul* new_mul(Expression* e1, Expression* e2) {
	return new Mul(e1,e2);
}
Div* new_div(Expression* e1, Expression* e2) {
	return new Div(e1,e2);
}
Pow* new_pow(Expression* e1, Expression* e2) {
	return new Pow(e1,e2);
}
MatrixMul* new_matrixmul(Expression* e1, Expression* e2) {
	return new MatrixMul(e1,e2);
}
MatrixDiv* new_matrixdiv(Expression* e1, Expression* e2) {
	return new MatrixDiv(e1,e2);
}
MatrixPow* new_matrixpow(Expression* e1, Expression* e2) {
	return new MatrixPow(e1,e2);
}
LECmp* new_le(Expression* e1, Expression* e2) {
	return new LECmp(e1,e2);
}
EQCmp* new_eq(Expression* e1, Expression* e2) {
	return new EQCmp(e1,e2);
}
NQCmp* new_nq(Expression* e1, Expression* e2) {
	return new NQCmp(e1,e2);
}
LTCmp* new_lt(Expression* e1, Expression* e2) {
	return new LTCmp(e1,e2);
}

Neg* new_neg(Expression* e) {
	return new Neg(e);
}

Assign * new_assign(Symbol var, Expression* e) {
	return new Assign(var, e);
}

Call* new_call(Symbol function, Expressions* actual) {
	return new Call(function, actual);
}

StatementBlock* new_statement_block(StatementList* stmts) {
	return new StatementBlock(stmts);
}

WhileLoopStmt* new_while(Expression* pred, StatementBlock* body) {
	return new WhileLoopStmt(pred, body);	
}


ReturnStmt* new_return(Expression* ret) {
	return new ReturnStmt(ret);	
}

ConditionalStmt* new_if(Expression* pred, StatementBlock* body, StatementBlock* f) {
	return new ConditionalStmt(pred, body, f);
}

ExpressionStmt* new_expr(Expression* expr) {
	return new ExpressionStmt(expr);
}

LocalVarDeclStmt* new_local(Symbol type, Symbol name, Expression* init) {
	return new LocalVarDeclStmt(type, name, init);
}


Formal* new_formal(Symbol type, Symbol name) {
	return new Formal(name, type);
}

FunctionDeclaration* new_function(Symbol ret, Symbol name, FormalList* formals, StatementBlock* body) {
	return new FunctionDeclaration(ret, name, formals, body);
}

Program* new_program(DeclarationList* declarations) {
	return new Program(declarations); 
}

Expressions* new_expressions(Expression* e, Expressions* old) {
	if(old == NULL) 
		old = new Expressions();
	old->push_back(e);
	return old;
}

Expressions* empty_expr_list() {
	return new Expressions();
}

FormalList* new_formals(Formal* f, FormalList* old) {
	if(old == NULL)
		old = new FormalList();
	old->push_back(f);
	return old;
}

FormalList* empty_formals() {
	return new FormalList();
}

DeclarationList* new_functions(FunctionDeclaration* d, DeclarationList* old) {
	if(old == NULL)
		old = new DeclarationList();
	old->push_back(d);
	return old;
}

StatementList* new_statements(Statement* s, StatementList* old) {
	if(old == NULL)
		old = new StatementList();
	old->push_back(s);
	return old;
}