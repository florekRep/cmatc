#include "declarations.h"
#include "cmat.tab.h" 
#include "tokens.h"
#include "cgen.h"
#include "sem_analyzer.h"

int curr_lineno = 1;
char *curr_filename = "<stdin>";	// this name is arbitrary
FILE *fin;							// This is the file pointer from which the lexer reads its input.

extern Program* ast_root;
extern int omerrs;
extern int yyparse();

void stop_execution() {
	system("pause");
	exit(1);
}

void usage() {
	cerr << "Usage: cmatc file [-O|-d]" << endl;
	cerr << "\t -O optimize bit code " << endl;
	cerr << "\t -d dump bit code " << endl;
	stop_execution();
}

bool optimise(int argc, char** argv) {
	if (argc == 3) return strcmp(argv[2], "-O") == 0;
	if (argc > 3) return strcmp(argv[2], "-O") == 0 || strcmp(argv[3], "-O") == 0 ;
}

bool dump(int argc, char** argv) {
	if (argc == 3) return strcmp(argv[2], "-d") == 0;
	if (argc > 3) return strcmp(argv[2], "-d") == 0 || strcmp(argv[3], "-d") == 0 ;
}

int main(int argc, char** argv) {
	
	if(argc < 2) usage();
	fin = fopen(argv[1], "r");
	if (fin == NULL) {
		cerr << "Could not open input file " << argv[1] << endl;
		stop_execution();
	}

	yyparse();
	fclose(fin);
	if(omerrs != 0) {
		cerr << "Parsing error, compilation halted" << endl;
		stop_execution();
	}

	Semant_analyzer semant;
	semant.run(ast_root);
	if(semant.errors > 0) {
		cerr <<"Semant error, compilation halted" << endl;
		stop_execution();
	}

	LLVM_cgen gen(optimise(argc, argv));
	gen.run(ast_root, dump(argc, argv));

	system("pause");
	return 0;
}
