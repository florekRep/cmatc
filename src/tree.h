#ifndef TREE_H
#define TREE_H
#include "symboltable.h"
#include "declarations.h"
#include "ivisitor.h"
#include "ISemant.h"

// base tree class
class Node {
protected:
	int line_number;
public:
	Node();
	virtual ~Node() {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
	int get_line_number() { return line_number; }
	void set_line_number(Node* n) {
		line_number = n->line_number;
	}
};


///////////////// EXPRESSIONS ///////////// (typed)
class Expression: public Node { 
public:
	Symbol type;
	Expression() { type = NULL; }
};
typedef vector<Expression*> Expressions;

//CONSTANTS
class FloatingPoint : public Expression {
public:
	Symbol value;
	FloatingPoint(Symbol v): value(v) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class Integer: public Expression {
public:
	Symbol value;
	Integer(Symbol v): value(v) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class String: public Expression {
public:
	Symbol value;
	String(Symbol v): value(v) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class Boolean: public Expression {
public:
	bool val;
	Boolean(bool b): val(b) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class Identifier: public Expression {
public:
	Symbol name;
	Identifier(Symbol n): name(n) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

//MATRIX 

typedef vector<Expressions*> MatrixRows; 
class Matrix: public Expression {
public:
	MatrixRows* matrix;
	Matrix(MatrixRows* m): matrix(m) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

//ARITHMETIC EXPR
class Pow: public Expression {
public:
	Expression* e1;
	Expression* e2;
	Pow(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};
class Add: public Expression {
public:
	Expression* e1;
	Expression* e2;
	Add(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class Sub: public Expression {
public:
	Expression* e1;
	Expression* e2;
	Sub(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};
class Mul: public Expression {
public:
	Expression* e1;
	Expression* e2;
	Mul(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};
class Div: public Expression {
public:
	Expression* e1;
	Expression* e2;
	Div(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class MatrixMul: public Expression {
public:
	Expression* e1;
	Expression* e2;
	MatrixMul(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};
class MatrixDiv: public Expression {
public:
	Expression* e1;
	Expression* e2;
	MatrixDiv(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class MatrixPow: public Expression {
public:
	Expression* e1;
	Expression* e2;
	MatrixPow(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};


class LTCmp: public Expression {
public:
	Expression* e1;
	Expression* e2;
	LTCmp(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};
class EQCmp: public Expression {
public:
	Expression* e1;
	Expression* e2;
	EQCmp(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};
class NQCmp: public Expression {
public:
	Expression* e1;
	Expression* e2;
	NQCmp(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};
class LECmp: public Expression {
public:
	Expression* e1;
	Expression* e2;
	LECmp(Expression* l, Expression* r): e1(l), e2(r) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class Neg: public Expression {
public:
	Expression* e;
	Neg(Expression *exp): e(exp) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

//Asignment
class Assign: public Expression {
public:
	Symbol var;
	Expression* e;
	Assign(Symbol name, Expression* expr): var(name), e(expr) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

//Function call
class Call: public Expression {
public:
	Symbol name;
	Expressions* actual;
	Call(Symbol function, Expressions* act): name(function), actual(act) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};


///////////// STATEMENTS //////////// (not typed)
class Statement : public Node {};

typedef vector<Statement*> StatementList;


class StatementBlock: public Node {
public:
	StatementList* statements;
	StatementBlock(StatementList* stmts): statements(stmts) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class WhileLoopStmt: public Statement {
public:
	Expression* pred;
	StatementBlock* body;
	WhileLoopStmt(Expression* p, StatementBlock *b): pred(p), body(b) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class ReturnStmt: public Statement {
public:
	Expression* ret;
	ReturnStmt(Expression* res): ret(res) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class ConditionalStmt: public Statement {
public:
	Expression* pred;
	StatementBlock* body;
	StatementBlock* false_cond;
	ConditionalStmt(Expression* p, StatementBlock* b, StatementBlock* f) : pred(p), body(b), false_cond(f) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class ExpressionStmt: public Statement {
public:
	Expression* expr;
	ExpressionStmt(Expression* e): expr(e) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

class LocalVarDeclStmt: public Statement {
public:
	Symbol type;
	Symbol name;
	Expression* init;
	LocalVarDeclStmt(Symbol t, Symbol n, Expression* i): type(t), name(n), init(i) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

///////// TOP LEVEL DECLARATIONS /////

class Formal: public Node {
public:
	Symbol name;
	Symbol type_decl;
	Formal(Symbol n, Symbol t): name(n), type_decl(t) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
};

typedef vector<Formal*> FormalList;

class FunctionDeclaration: public Node {
public:
	Symbol name;
	FormalList* formals;
	Symbol return_type;
	StatementBlock* body;
	FunctionDeclaration(Symbol r, Symbol n, FormalList* f, StatementBlock* b): return_type(r), name(n), formals(f), body(b) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};

// TOP CLASS
typedef vector<FunctionDeclaration*> DeclarationList;

class Program : public Node {
public:
	DeclarationList* declarations;
	Program(DeclarationList* d): declarations(d) {}
	virtual llvm::Value* accept(IVisitor* visitor) { return visitor->visit(this); }
	virtual void accept(ISemant * s) { return s->semant(this); }
};


//API
FloatingPoint* new_float(Symbol value);
Integer* new_int(Symbol value);
String* new_string(Symbol value);
Boolean* new_bool(bool value);
Identifier* new_identifier(Symbol value);
MatrixRows* new_matrixRows(Expressions* row, MatrixRows* old);
Matrix* new_matrix(MatrixRows* m);
Add* new_add(Expression* e1, Expression* e2);
Sub* new_sub(Expression* e1, Expression* e2);
Mul* new_mul(Expression* e1, Expression* e2);
Div* new_div(Expression* e1, Expression* e2);
Pow* new_pow(Expression* e1, Expression* e2);
MatrixMul* new_matrixmul(Expression* e1, Expression* e2);
MatrixDiv* new_matrixdiv(Expression* e1, Expression* e2);
MatrixPow* new_matrixpow(Expression* e1, Expression* e2);
LECmp* new_le(Expression* e1, Expression* e2);
EQCmp* new_eq(Expression* e1, Expression* e2);
NQCmp* new_nq(Expression* e1, Expression* e2);
LTCmp* new_lt(Expression* e1, Expression* e2);
Neg* new_neg(Expression* e);
Assign * new_assign(Symbol var, Expression* e);
Call* new_call(Symbol function, Expressions* actual);
StatementBlock* new_block(StatementList* stmts);
WhileLoopStmt* new_while(Expression* pred, StatementBlock* body);
ReturnStmt* new_return(Expression* ret);
ConditionalStmt* new_if(Expression* pred, StatementBlock* body, StatementBlock* f);
ExpressionStmt* new_expr(Expression* expr);
LocalVarDeclStmt* new_local(Symbol type, Symbol name, Expression* init);
Formal* new_formal(Symbol type, Symbol name);
FunctionDeclaration* new_function(Symbol ret, Symbol name, FormalList* formals, StatementBlock* body);
Program* new_program(DeclarationList* declarations);
Expressions* new_expressions(Expression* e, Expressions* old);
Expressions* empty_expr_list();
FormalList* new_formals(Formal* f, FormalList* old);
FormalList* empty_formals();
DeclarationList* new_functions(FunctionDeclaration* d, DeclarationList* old);
StatementList* new_statements(Statement* s, StatementList* old);
StatementBlock* new_statement_block(StatementList* s);

#endif