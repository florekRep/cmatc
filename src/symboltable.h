#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

#include "declarations.h"

class Entry {
protected:
	string str;   
	int index;   
public:
	Entry(char* s, int i): str(s), index(i) {}
	int equal_string(char* s) const {
		return str == s;
	}
	int equal_string(string s) const {
		return str == s;
	}
	bool equal_index(int ind) const { 
		return ind == index;
	}
	string get_string() const {
		return str;
	}
};

class SymbolTable {
	std::list<Entry*> tbl;
	int index;
public:
	SymbolTable(): index(0) {} 
	Entry* add_string(char* s);
	Entry* lookup(int index);
	Entry* lookup(char* s);
	Entry* lookup(string s);
};

typedef Entry* Symbol;

extern ostream& operator<<(ostream& os, const Entry& sym);
extern ostream& operator<<(ostream& os, Symbol sym);

extern SymbolTable idtable;
extern SymbolTable inttable;
extern SymbolTable fptable;
extern SymbolTable stringtable;

#endif //SYMBOL_TABLE_H