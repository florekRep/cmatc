#ifndef TOKENS_H
#define TOKENS_H

#include "declarations.h"

union YYSTYPE;

string token_to_string(int tok);
void dump_token(ostream& out, int lineno, int tok, YYSTYPE yylval);
void print_token(ostream& out);

#endif