#ifndef CGEN_H
#define CGEN_H

#include "ivisitor.h"
#include "symboltable.h"
#include "llvm/PassManager.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"

class LLVM_cgen : public IVisitor {
	llvm::Module * module;
	llvm::IRBuilder<> Builder;
	llvm::Function* currentFunction;
	std::map<Symbol, llvm::AllocaInst*> Variables;
	std::map<Symbol, Symbol> VariableTypes;
	llvm::FunctionPassManager* fpm;
	llvm::StructType *intMatrix;
	llvm::StructType *fpMatrix;
	bool optimize;

	void InstallMatrixStructs();
	void InstallRuntimeIM(llvm::ExecutionEngine*);
	void InstallRuntimeDM(llvm::ExecutionEngine*);
	void CreateArugmentAllocas(FunctionDeclaration* fd);
	llvm::Value* ErrorV(const char* msg);
	llvm::AllocaInst* CreateEntryBlockAlloca(Symbol variable, llvm::Type* type);
	llvm::AllocaInst* CreateEntryBlockAlloca(llvm::Type* type);
	llvm::Value* GetDefaultValue(Symbol type);
	llvm::Type* GetType(Symbol type);
	void InstallTypes();
	bool isMatrix(Symbol type);
	bool hasRet(FunctionDeclaration* fd);
public:
	LLVM_cgen(bool optimize = false);
	void dump();
	void run(Program*, bool dump = false);

	llvm::ExecutionEngine* LLVM_cgen::BuildEngine();

	//Visitor Iface
	llvm::Value* visit(Program *p);
	llvm::Value* visit(FunctionDeclaration* fd);
	llvm::Value* visit(LocalVarDeclStmt* lv);
	llvm::Value* visit(ExpressionStmt *p);
	llvm::Value* visit(ConditionalStmt *p);
	llvm::Value* visit(ReturnStmt *p);
	llvm::Value* visit(WhileLoopStmt *p);
	llvm::Value* visit(StatementBlock* sb);
	llvm::Value* visit(Call *p);
	llvm::Value* visit(Assign *p);
	llvm::Value* visit(Neg * d);
	llvm::Value* visit(LECmp * d);
	llvm::Value* visit(NQCmp * d);
	llvm::Value* visit(EQCmp * d);
	llvm::Value* visit(LTCmp * d);

	llvm::Value* visit(Add* a);
    llvm::Value* visit(Sub * s);
    llvm::Value* visit(Mul * m);
    llvm::Value* visit(Div * d);
    llvm::Value* visit(Pow * d);
    llvm::Value* visit(MatrixMul * m);
    llvm::Value* visit(MatrixDiv * d);
    llvm::Value* visit(MatrixPow * d);

	llvm::Value* visit(Integer* i);
    llvm::Value* visit(Identifier* i);
    llvm::Value* visit(Boolean* i);
	llvm::Value* visit(FloatingPoint* i);
	llvm::Value* visit(Matrix* m);

	llvm::Value* visit(Node* n);
};

#endif