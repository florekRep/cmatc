#include "symboltable.h"

Entry* SymbolTable::add_string(char* s) {
	for(list<Entry*>::iterator it = tbl.begin(); it != tbl.end(); ++it) {
		if((*it)->equal_string(s))
			return (*it);
	}
	Entry* e = new Entry(s, index++);
	tbl.push_back(e);
	return e;
}

Entry* SymbolTable::lookup(char* s) {
	for(list<Entry*>::iterator it = tbl.begin(); it != tbl.end(); ++it) {
		if((*it)->equal_string(s))
			return (*it);
	}
	return NULL;
}

Entry* SymbolTable::lookup(string s) {
	for(list<Entry*>::iterator it = tbl.begin(); it != tbl.end(); ++it) {
		if((*it)->equal_string(s))
			return (*it);
	}
	return NULL;
}

Entry* SymbolTable::lookup(int index) {
	for(list<Entry*>::iterator it = tbl.begin(); it != tbl.end(); ++it) {
		if((*it)->equal_index(index))
			return (*it);
	}
	return NULL;
}


ostream& operator<<(ostream& os, const Entry& sym) {
	return os << sym.get_string();
}
ostream& operator<<(ostream& os, Symbol sym) {
	return os << (*sym);
}

// global symbol tables
SymbolTable idtable;
SymbolTable inttable;
SymbolTable fptable;
SymbolTable stringtable;
