#ifndef MATRIX_RT_H
#define MATRIX_RT_H

//Int matrix 
struct matrix_int_t {
	int rows;
	int cols;
	int* data;
};

void addIM(struct matrix_int_t* r, struct matrix_int_t a, struct matrix_int_t b);
void subIM(struct matrix_int_t* r, struct matrix_int_t a, struct matrix_int_t b);
void mulIM(struct matrix_int_t* r, struct matrix_int_t a, struct matrix_int_t b);
void negIM(struct matrix_int_t* r, struct matrix_int_t a);
int eqIM(struct matrix_int_t a, struct matrix_int_t b);
void mmulIM(struct matrix_int_t* r, struct matrix_int_t a, int b);
void mdivIM(struct matrix_int_t* r, struct matrix_int_t a, int b);
void cloneIM(struct matrix_int_t* dst, struct matrix_int_t src);
void printIM(struct matrix_int_t m);

//Double matrix
struct matrix_fp_t {
	int rows;
	int cols;
	double* data;
};

void addDM(struct matrix_fp_t* r, struct matrix_fp_t a, struct matrix_fp_t b);
void subDM(struct matrix_fp_t* r, struct matrix_fp_t a, struct matrix_fp_t b);
void mulDM(struct matrix_fp_t* r, struct matrix_fp_t a, struct matrix_fp_t b);
void negDM(struct matrix_fp_t* r, struct matrix_fp_t a);
int eqDM(struct matrix_fp_t a, struct matrix_fp_t b);
void mmulDM(struct matrix_fp_t* r, struct matrix_fp_t a, double b);
void mdivDM(struct matrix_fp_t* r, struct matrix_fp_t a, double b);
void cloneDM(struct matrix_fp_t* dst, struct matrix_fp_t src);
void printDM(struct matrix_fp_t m);

#endif