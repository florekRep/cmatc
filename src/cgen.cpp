#include "llvm/Analysis/Passes.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/ExecutionEngine/JIT.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Verifier.h"
#include "llvm/PassManager.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "Ivisitor.h"
#include "tree.h"
#include <cctype>
#include <cstdio>
#include <map>
#include <string>
#include <vector>
#include <iostream>

#include "matrix-rt.h"
#include "cgen.h"

using namespace llvm;
using namespace std;

bool hasRet(FunctionDeclaration*);
extern Symbol Void;
extern Symbol Int;
extern Symbol Double;
extern Symbol IntM;
extern Symbol DoubleM;
extern Symbol Str;
extern Symbol Bool;

Value* LLVM_cgen::ErrorV(const char* msg) { 
	cerr << "Error: " << msg << endl; return 0;
}

LLVM_cgen::LLVM_cgen(bool opt): Builder(getGlobalContext()), optimize(opt) {
	InitializeNativeTarget();
	module = new Module("JIT compiler", getGlobalContext());
}

//Runs in JIT mode
void LLVM_cgen::run(Program* root, bool dumpBit) {
	ExecutionEngine* ee = BuildEngine();
	InstallMatrixStructs();
	InstallRuntimeIM(ee);
	InstallRuntimeDM(ee);
	Function* f = (Function*) visit(root);
	verifyModule(*module);
	if(dumpBit) {
		cout << "LLVM bitcode dump: " << endl;
		dump();
	}
	cout << "Trying to run ...: " << endl;
	std::vector<GenericValue> args(0);
	ee->runFunction(f,args);
}

ExecutionEngine* LLVM_cgen::BuildEngine() {
	std::string error;
	ExecutionEngine * engine = EngineBuilder(module).setErrorStr(&error).create(); 
	if(!engine) {
		cerr << "Error initializing ExecutionEngine: " << error << endl;
	}
	fpm = new FunctionPassManager(module);
	//Optimization passes
	module->setDataLayout(engine->getDataLayout());
	fpm->add(new DataLayoutPass(module));
	fpm->add(createBasicAliasAnalysisPass());
	fpm->add(createPromoteMemoryToRegisterPass());
	fpm->add(createInstructionCombiningPass());
	fpm->add(createReassociatePass());
	fpm->add(createGVNPass());
	fpm->add(createCFGSimplificationPass());
	fpm->doInitialization();

	return engine;
}

void LLVM_cgen::InstallMatrixStructs() {
	vector<Type*> matrix_int_type;
	matrix_int_type.push_back(Type::getInt32Ty(getGlobalContext()));
	matrix_int_type.push_back(Type::getInt32Ty(getGlobalContext()));
	matrix_int_type.push_back(Type::getInt32PtrTy(getGlobalContext()));
	intMatrix = StructType::create(getGlobalContext(), matrix_int_type, "matrix_int_t");

	vector<Type*> matrix_double_type;
	matrix_double_type.push_back(Type::getInt32Ty(getGlobalContext()));
	matrix_double_type.push_back(Type::getInt32Ty(getGlobalContext()));
	matrix_double_type.push_back(Type::getDoublePtrTy(getGlobalContext()));
	fpMatrix = StructType::create(getGlobalContext(), matrix_double_type, "matrix_fp_t");
}

void LLVM_cgen::dump() {
	module->dump();
}

//Will install runtime library (for math operations)
void LLVM_cgen::InstallRuntimeIM(ExecutionEngine* ee) {
	vector<Type*> malloc_types;
	malloc_types.push_back(Type::getInt32Ty(getGlobalContext()));
	FunctionType* malloc_ft = FunctionType::get(Type::getInt32PtrTy(getGlobalContext()), malloc_types, false);
	Function* malloc_f = Function::Create(malloc_ft, Function::ExternalLinkage, "mallocI", module);
	ee->addGlobalMapping(malloc_f, &malloc);

	vector<Type*> printTypes;
	printTypes.push_back(intMatrix);
	ArrayRef<Type*> printArgs(printTypes);
	FunctionType * printIM_type = FunctionType::get(Builder.getVoidTy(), printArgs, false);
	Function* printIM_fun = Function::Create(printIM_type, Function::ExternalLinkage, "printIM", module);
	ee->addGlobalMapping(printIM_fun, &printIM);

	vector<Type*> cloneIM_types;
	cloneIM_types.push_back(intMatrix->getPointerTo());
	cloneIM_types.push_back(intMatrix);
	FunctionType* cloneIM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), cloneIM_types, false);
	Function* cloneIM_f = Function::Create(cloneIM_ft, Function::ExternalLinkage, "cloneIM", module);
	ee->addGlobalMapping(cloneIM_f, &cloneIM);

	vector<Type*> arithmIM_types;
	arithmIM_types.push_back(intMatrix->getPointerTo());
	arithmIM_types.push_back(intMatrix);
	arithmIM_types.push_back(intMatrix);

	FunctionType* addIM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), arithmIM_types, false);
	Function* addIM_f = Function::Create(addIM_ft, Function::ExternalLinkage, "addIM", module);
	ee->addGlobalMapping(addIM_f, &addIM);

	FunctionType* subIM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), arithmIM_types, false);
	Function* subIM_f = Function::Create(subIM_ft, Function::ExternalLinkage, "subIM", module);
	ee->addGlobalMapping(subIM_f, &subIM);

	FunctionType* mulIM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), arithmIM_types, false);
	Function* mulIM_f = Function::Create(mulIM_ft, Function::ExternalLinkage, "mulIM", module);
	ee->addGlobalMapping(mulIM_f, &mulIM);

	vector<Type*> matrixIM_op_types;
	matrixIM_op_types.push_back(intMatrix->getPointerTo());
	matrixIM_op_types.push_back(intMatrix);
	matrixIM_op_types.push_back(Type::getInt32Ty(getGlobalContext()));

	FunctionType* mmulIM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), matrixIM_op_types, false);
	Function* mmulIM_f = Function::Create(mmulIM_ft, Function::ExternalLinkage, "mmulIM", module);
	ee->addGlobalMapping(mmulIM_f, &mmulIM);

	FunctionType* mdivIM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), matrixIM_op_types, false);
	Function* mdivIM_f = Function::Create(mdivIM_ft, Function::ExternalLinkage, "mdivIM", module);
	ee->addGlobalMapping(mdivIM_f, &mdivIM);

	vector<Type*> cmpIM_op_types;
	cmpIM_op_types.push_back(intMatrix);
	cmpIM_op_types.push_back(intMatrix);

	FunctionType* eqIM_ft = FunctionType::get(Type::getInt32Ty(getGlobalContext()), cmpIM_op_types, false);
	Function* eqIM_f = Function::Create(eqIM_ft, Function::ExternalLinkage, "eqIM", module);
	ee->addGlobalMapping(eqIM_f, &eqIM);

	vector<Type*> negIM_types;
	negIM_types.push_back(intMatrix->getPointerTo());
	negIM_types.push_back(intMatrix);
	
	FunctionType* negIM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), negIM_types, false);
	Function* negIM_f = Function::Create(negIM_ft, Function::ExternalLinkage, "negIM", module);
	ee->addGlobalMapping(negIM_f, &negIM);
}

void LLVM_cgen::InstallRuntimeDM(ExecutionEngine* ee) {
	vector<Type*> malloc_types;
	malloc_types.push_back(Type::getInt32Ty(getGlobalContext()));
	FunctionType* malloc_ft = FunctionType::get(Type::getDoublePtrTy(getGlobalContext()), malloc_types, false);
	Function* malloc_f = Function::Create(malloc_ft, Function::ExternalLinkage, "mallocD", module);
	ee->addGlobalMapping(malloc_f, &malloc);

	vector<Type*> printTypes;
	printTypes.push_back(fpMatrix);
	ArrayRef<Type*> printArgs(printTypes);
	FunctionType * printDM_type = FunctionType::get(Builder.getVoidTy(), printArgs, false);
	Function* printDM_fun = Function::Create(printDM_type, Function::ExternalLinkage, "printDM", module);
	ee->addGlobalMapping(printDM_fun, &printDM);

	vector<Type*> cloneDM_types;
	cloneDM_types.push_back(fpMatrix->getPointerTo());
	cloneDM_types.push_back(fpMatrix);
	FunctionType* cloneDM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), cloneDM_types, false);
	Function* cloneDM_f = Function::Create(cloneDM_ft, Function::ExternalLinkage, "cloneDM", module);
	ee->addGlobalMapping(cloneDM_f, &cloneDM);

	vector<Type*> arithmDM_types;
	arithmDM_types.push_back(fpMatrix->getPointerTo());
	arithmDM_types.push_back(fpMatrix);
	arithmDM_types.push_back(fpMatrix);

	FunctionType* addDM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), arithmDM_types, false);
	Function* addDM_f = Function::Create(addDM_ft, Function::ExternalLinkage, "addDM", module);
	ee->addGlobalMapping(addDM_f, &addDM);

	FunctionType* subDM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), arithmDM_types, false);
	Function* subDM_f = Function::Create(subDM_ft, Function::ExternalLinkage, "subDM", module);
	ee->addGlobalMapping(subDM_f, &subDM);

	FunctionType* mulDM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), arithmDM_types, false);
	Function* mulDM_f = Function::Create(mulDM_ft, Function::ExternalLinkage, "mulDM", module);
	ee->addGlobalMapping(mulDM_f, &mulDM);

	vector<Type*> matrixDM_op_types;
	matrixDM_op_types.push_back(fpMatrix->getPointerTo());
	matrixDM_op_types.push_back(fpMatrix);
	matrixDM_op_types.push_back(Type::getDoubleTy(getGlobalContext()));

	FunctionType* mmulDM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), matrixDM_op_types, false);
	Function* mmulDM_f = Function::Create(mmulDM_ft, Function::ExternalLinkage, "mmulDM", module);
	ee->addGlobalMapping(mmulDM_f, &mmulDM);

	FunctionType* mdivDM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), matrixDM_op_types, false);
	Function* mdivDM_f = Function::Create(mdivDM_ft, Function::ExternalLinkage, "mdivDM", module);
	ee->addGlobalMapping(mdivDM_f, &mdivDM);

	vector<Type*> cmpDM_op_types;
	cmpDM_op_types.push_back(fpMatrix);
	cmpDM_op_types.push_back(fpMatrix);

	FunctionType* eqDM_ft = FunctionType::get(Type::getInt32Ty(getGlobalContext()), cmpDM_op_types, false);
	Function* eqDM_f = Function::Create(eqDM_ft, Function::ExternalLinkage, "eqDM", module);
	ee->addGlobalMapping(eqDM_f, &eqDM);

	vector<Type*> negDM_types;
	negDM_types.push_back(fpMatrix->getPointerTo());
	negDM_types.push_back(fpMatrix);
	
	FunctionType* negDM_ft = FunctionType::get(Type::getVoidTy(getGlobalContext()), negDM_types, false);
	Function* negDM_f = Function::Create(negDM_ft, Function::ExternalLinkage, "negDM", module);
	ee->addGlobalMapping(negDM_f, &negDM);
}

AllocaInst* LLVM_cgen::CreateEntryBlockAlloca(Symbol variable, Type* type) {
	IRBuilder<> TmpBuilder(&currentFunction->getEntryBlock(),
		currentFunction->getEntryBlock().begin());
	return TmpBuilder.CreateAlloca(type, 0, (variable->get_string().c_str()));
}

AllocaInst* LLVM_cgen::CreateEntryBlockAlloca(Type* type) {
	IRBuilder<> TmpBuilder(&currentFunction->getEntryBlock(),
		currentFunction->getEntryBlock().begin());
	return TmpBuilder.CreateAlloca(type,0);
}

Value* LLVM_cgen::GetDefaultValue(Symbol type) {
	if(type==Int || type==Bool)
		return ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0, true);
	if(type==Double)
		return ConstantFP::get(Type::getDoubleTy(getGlobalContext()), 0.0);
	if(type==IntM) {
		vector<Constant*> c;
		c.push_back(ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0, true));
		c.push_back(ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0, true));
		c.push_back(ConstantPointerNull::get(Type::getInt32PtrTy(getGlobalContext())));
		return ConstantStruct::get(intMatrix, c);
	}
	if(type==DoubleM) {
		vector<Constant*> c;
		c.push_back(ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0, true));
		c.push_back(ConstantInt::get(Type::getInt32Ty(getGlobalContext()), 0, true));
		c.push_back(ConstantPointerNull::get(Type::getDoublePtrTy(getGlobalContext())));
		return ConstantStruct::get(fpMatrix, c);
	}
}

Type* LLVM_cgen::GetType(Symbol type) {
	if(type==Int || type==Bool)
		return Type::getInt32Ty(getGlobalContext());
	if(type==Double)
		return Type::getDoubleTy(getGlobalContext());
	if(type==Void)
		return Type::getVoidTy(getGlobalContext());
	if(type==IntM)
		return intMatrix;
	if(type==DoubleM)
		return fpMatrix;
}

void LLVM_cgen::CreateArugmentAllocas(FunctionDeclaration* fd) {
	Function::arg_iterator AI = currentFunction->arg_begin();
	for(int i = 0; i < fd->formals->size(); ++i) {
		// Create an alloca for this variable.
		Formal * f = (*fd->formals)[i];
		AllocaInst *Alloca = CreateEntryBlockAlloca(f->name, GetType(f->type_decl));
		// Store the initial value into the alloca.
		Builder.CreateStore(AI, Alloca);
		// Add arguments to variable symbol table.
		Variables[f->name] = Alloca;
		VariableTypes[f->name] = f->type_decl;
		++AI;
	}
}

bool LLVM_cgen::isMatrix(Symbol type) {
	return type == IntM || type == DoubleM;
}

bool LLVM_cgen::hasRet(FunctionDeclaration* fd) {
	for(int i = 0; i < fd->body->statements->size(); i++) {
		Statement* s = (*fd->body->statements)[i];
		if(typeid(*s) == typeid(ReturnStmt))
			return true;
	}
	return false;
}
// ======================== AST TRAVERSAL ========================
//Shouldn't be called
Value* LLVM_cgen::visit(Node* n) {
	return ErrorV("Unexpected symbol");
}

//Program
Value* LLVM_cgen::visit(Program *p) {
	Value* main;
	// For each function, codegen it
	for(int i = 0; i < p->declarations->size(); i++) {
		FunctionDeclaration * fd = (*p->declarations)[i];
		Function * f = (Function*)fd->accept(this);
		if( fd != NULL && fd->name->equal_string("main")) {		//save pointer to main function
			main = f;
		}
		if(optimize) fpm->run(*f);
	}
	return main;
}

Value* LLVM_cgen::visit(FunctionDeclaration* fd) {
	// Handle function types
	Type* returnType = GetType(fd->return_type);
	vector<Type*> params;
	for(int i = 0; i < fd->formals->size(); i++) {
		params.push_back(GetType((*fd->formals)[i]->type_decl));
	}
	FunctionType * functionType = FunctionType::get(returnType, params, false);
	Function * f = Function::Create(functionType, Function::ExternalLinkage, fd->name->get_string(), module);

	currentFunction = f;

	int i = 0;
	for(Function::arg_iterator it = f->arg_begin(); i < fd->formals->size(); ++i, ++it) {
		it->setName((*fd->formals)[i]->name->get_string());
		//Arguments[(*fd->formals)[i]->name] = it;
	}

	BasicBlock* BB = BasicBlock::Create(getGlobalContext(), "entry", f);
	Builder.SetInsertPoint(BB);

	Variables.clear(); VariableTypes.clear();
	CreateArugmentAllocas(fd);

	//codegen body
	fd->body->accept(this);

	//if has no return than insert void
	if(!hasRet(fd)) Builder.CreateRetVoid();
	
	verifyFunction(*f);
	return f;
}

Value* LLVM_cgen::visit(ReturnStmt *p) {
	Value* v = p->ret->accept(this);	//eval ret expr
	Builder.CreateRet(v);				//create return
	return v;
}

Value* LLVM_cgen::visit(LocalVarDeclStmt * localVar) {
	Value * initVal;
	if(localVar->init != NULL) {
		initVal = localVar->init->accept(this);
	} else {
		initVal = GetDefaultValue(localVar->type);
	}
	AllocaInst * Alloca = CreateEntryBlockAlloca(localVar->name, GetType(localVar->type));
	Builder.CreateStore(initVal, Alloca);
	Variables[localVar->name] = Alloca;
	VariableTypes[localVar->name] = localVar->type;

	return initVal;
}

Value* LLVM_cgen::visit(Identifier* i) {
	Value * val = Variables[i->name];		//Variable have higher scope
	return Builder.CreateLoad(val, i->name->get_string().c_str());
}

Value* LLVM_cgen::visit(StatementBlock* sb) {
	for(int i = 0; i < sb->statements->size(); ++i) {
		Statement * s = (*sb->statements)[i];
		s->accept(this);
	}
	return (Value*)0;
}

Value* LLVM_cgen::visit(ExpressionStmt* sb) {
	return sb->expr->accept(this);
}

Value* LLVM_cgen::visit(Assign *p) {
	Value * v = p->e->accept(this);
	AllocaInst* var = Variables[p->var];
	if(isMatrix(VariableTypes[p->var])) {
		Function * toCall = (VariableTypes[p->var] == IntM) ? module->getFunction("cloneIM") : module->getFunction("cloneDM");
		vector<Value*> args;
		args.push_back(var);
		args.push_back(v);
		Builder.CreateCall(toCall, args);
	}
	else {
		Builder.CreateStore(v, var);
	}
	return v;
}

Value* LLVM_cgen::visit(Call* c) {
	Function * toCall = module->getFunction(c->name->get_string());
	vector<Value*> args;
	for(int i = 0; i < c->actual->size(); i++) {
		Value* v = (*c->actual)[i]->accept(this);
		args.push_back(v);
	}
	return Builder.CreateCall(toCall, args);
}

//Constants
Value* LLVM_cgen::visit(Integer* i) {
	int value = atoi(i->value->get_string().c_str());		
	return ConstantInt::get(Type::getInt32Ty(getGlobalContext()), value, true);
}

Value* LLVM_cgen::visit(Boolean* i){
	int value = i->val;		
	return ConstantInt::get(Type::getInt32Ty(getGlobalContext()), value, true);
}

Value* LLVM_cgen::visit(FloatingPoint* i) {
	double value = atof(i->value->get_string().c_str());
	return ConstantFP::get(Type::getDoubleTy(getGlobalContext()), value);
}

Value* LLVM_cgen::visit(Matrix* m) {
	int rows = m->matrix->size();
	int cols = ((*m->matrix)[0])->size();

	
	Value* newMatrix = (m->type == IntM) ? CreateEntryBlockAlloca(intMatrix) : CreateEntryBlockAlloca(fpMatrix);
	
	AllocaInst* x;
	Builder.CreateStore(
		ConstantInt::get(Type::getInt32Ty(getGlobalContext()), rows, true),
		Builder.CreateStructGEP(newMatrix, 0));
	Builder.CreateStore(
		ConstantInt::get(Type::getInt32Ty(getGlobalContext()), cols, true),
		Builder.CreateStructGEP(newMatrix, 1));

	vector<Value*> args;
	Function * toCall;
	if(m->type == IntM) {
		args.push_back(ConstantInt::get(Type::getInt32Ty(getGlobalContext()), cols*rows*sizeof(int), true));
		toCall = module->getFunction("mallocI");
	} else {
		args.push_back(ConstantInt::get(Type::getInt32Ty(getGlobalContext()), cols*rows*sizeof(double), true));
		toCall = module->getFunction("mallocD");
	}
	Value* data = Builder.CreateCall(toCall, args);	//call to create new matrix
	Builder.CreateStore(data, Builder.CreateStructGEP(newMatrix,2));

	for(int i = 0; i < rows; i++) {
		for(int j = 0; j < cols; j++) {
			Expression * e = (*(*m->matrix)[i])[j];
			Value * v = e->accept(this);
			Builder.CreateStore(v, Builder.CreateConstGEP1_32(data, i*cols + j));
		}
	}
	return Builder.CreateLoad(newMatrix);
}


//Arithmetics
Value* LLVM_cgen::visit(Add* a) {
	Value* left = a->e1->accept(this);
	Value* right = a->e2->accept(this);
	if(isMatrix(a->type)) {		
		Function * toCall;
		AllocaInst* tmp;
		if(a->type == IntM) {
			toCall = module->getFunction("addIM");
			tmp = CreateEntryBlockAlloca(intMatrix);
		} 
		if(a->type == DoubleM) {
			toCall = module->getFunction("addDM");
			tmp = CreateEntryBlockAlloca(fpMatrix);
		}
		vector<Value*> args;
		args.push_back(tmp);
		args.push_back(left);
		args.push_back(right);
		Builder.CreateCall(toCall, args);
		return Builder.CreateLoad(tmp);
	} else if(a->type == Int) {
		return Builder.CreateAdd(left, right, "add");
	} else if(a->type == Double) {
		return Builder.CreateFAdd(left, right, "add");
	}
}

Value* LLVM_cgen::visit(Sub * s) {
	Value* left = s->e1->accept(this);
	Value* right = s->e2->accept(this);
	if(isMatrix(s->type)) {		
		Function * toCall;
		AllocaInst* tmp;
		if(s->type == IntM) {
			toCall = module->getFunction("subIM");
			tmp = CreateEntryBlockAlloca(intMatrix);
		} 
		if(s->type == DoubleM) {
			toCall = module->getFunction("subDM");
			tmp = CreateEntryBlockAlloca(fpMatrix);
		}
		vector<Value*> args;
		args.push_back(tmp);
		args.push_back(left);
		args.push_back(right);
		Builder.CreateCall(toCall, args);
		return Builder.CreateLoad(tmp);
	} else if(s->type == Int) {
		return Builder.CreateSub(left, right, "sub");
	} else if(s->type == Double) {
		return Builder.CreateFSub(left, right, "sub");
	}
}

Value* LLVM_cgen::visit(Mul * m) {
	Value* left = m->e1->accept(this);
	Value* right = m->e2->accept(this);
	if(isMatrix(m->type)) {		
		Function * toCall;
		AllocaInst* tmp;
		if(m->type == IntM) {
			toCall = module->getFunction("mulIM");
			tmp = CreateEntryBlockAlloca(intMatrix);
		} 
		if(m->type == DoubleM) {
			toCall = module->getFunction("mulDM");
			tmp = CreateEntryBlockAlloca(fpMatrix);
		}
		vector<Value*> args;
		args.push_back(tmp);
		args.push_back(left);
		args.push_back(right);
		Builder.CreateCall(toCall, args);
		return Builder.CreateLoad(tmp);
	} else if(m->type == Int) {
		return Builder.CreateMul(left, right, "mul");
	} else if(m->type == Double) {
		return Builder.CreateFMul(left, right, "mul");
	}
}

Value* LLVM_cgen::visit(Div * d) {
	Value* left = d->e1->accept(this);
	Value* right = d->e2->accept(this);
	if(d->type == Int) {
		return Builder.CreateSDiv(left, right, "div");
	} else if(d->type == Double) {
		return Builder.CreateFDiv(left, right, "div");
	}
}

Value* LLVM_cgen::visit(Neg * n) {
	Value* left = n->e->accept(this);
	if(isMatrix(n->type)) {		
		Function * toCall;
		AllocaInst* tmp;
		if(n->type == IntM) {
			toCall = module->getFunction("negIM");
			tmp = CreateEntryBlockAlloca(intMatrix);
		} 
		if(n->type == DoubleM) {
			toCall = module->getFunction("negDM");
			tmp = CreateEntryBlockAlloca(fpMatrix);
		}
		vector<Value*> args;
		args.push_back(tmp);
		args.push_back(left);
		Builder.CreateCall(toCall, args);
		return Builder.CreateLoad(tmp);
	} else if(n->type == Int) {
		return Builder.CreateNeg(left, "neg");
	} else if(n->type == Double) {
		return Builder.CreateFNeg(left, "neg");
	}
}

Value* LLVM_cgen::visit(MatrixMul * m) {
	Value* left = m->e1->accept(this);
	Value* right = m->e2->accept(this);	
	Function * toCall;
	AllocaInst* tmp;
	if(m->type == IntM) {
		toCall = module->getFunction("mmulIM");
		tmp = CreateEntryBlockAlloca(intMatrix);
	} 
	if(m->type == DoubleM) {
		toCall = module->getFunction("mmulDM");
		tmp = CreateEntryBlockAlloca(fpMatrix);
	}
	vector<Value*> args;
	args.push_back(tmp);
	args.push_back(left);
	args.push_back(right);
	Builder.CreateCall(toCall, args);
	return Builder.CreateLoad(tmp);
}

Value* LLVM_cgen::visit(MatrixDiv * d) {
	Value* left = d->e1->accept(this);
	Value* right = d->e2->accept(this);	
	Function * toCall;
	AllocaInst* tmp;
	if(d->type == IntM) {
		toCall = module->getFunction("mdivIM");
		tmp = CreateEntryBlockAlloca(intMatrix);
	} 
	if(d->type == DoubleM) {
		toCall = module->getFunction("mdivDM");
		tmp = CreateEntryBlockAlloca(fpMatrix);
	}
	vector<Value*> args;
	args.push_back(tmp);
	args.push_back(left);
	args.push_back(right);
	Builder.CreateCall(toCall, args);
	return Builder.CreateLoad(tmp);
}

//	logic
Value* LLVM_cgen::visit(EQCmp * eq) {
	Value* left = eq->e1->accept(this);
	Value* right = eq->e2->accept(this);
	if(isMatrix(eq->e1->type)) {		
		Function * toCall;
		if(eq->e1->type == IntM) {
			toCall = module->getFunction("eqIM");
		} 
		if(eq->e1->type == DoubleM) {
			toCall = module->getFunction("eqDM");
		}
		vector<Value*> args;
		args.push_back(left);
		args.push_back(right);
		Value* result = Builder.CreateCall(toCall, args);
		return Builder.CreateICmpNE(result, GetDefaultValue(Int));
	} else if(eq->e1->type == Int) {
		return Builder.CreateICmpEQ(left, right, "eq");
	} else if(eq->e1->type == Double) {
		return Builder.CreateFCmpOEQ(left, right, "eq");
	}
}

Value* LLVM_cgen::visit(NQCmp * ne) {
	Value* left = ne->e1->accept(this);
	Value* right = ne->e2->accept(this);
	if(isMatrix(ne->e1->type)) {		
		Function * toCall;
		if(ne->e1->type == IntM) {
			toCall = module->getFunction("eqIM");
		} 
		if(ne->e1->type == DoubleM) {
			toCall = module->getFunction("eqDM");
		}
		vector<Value*> args;
		args.push_back(left);
		args.push_back(right);
		Value* result = Builder.CreateCall(toCall, args);
		return Builder.CreateICmpEQ(result, GetDefaultValue(Int));
	} else if(ne->e1->type == Int) {
		return Builder.CreateICmpNE(left, right, "nq");
	} else if(ne->e1->type == Double) {
		return Builder.CreateFCmpONE(left, right, "nq");
	}
}

Value* LLVM_cgen::visit(LTCmp * lt) {
	Value* left = lt->e1->accept(this);
	Value* right = lt->e2->accept(this);
	if(lt->e1->type == Int) {
		return Builder.CreateICmpSLT(left, right, "lt");
	} else if(lt->e1->type == Double) {
		return Builder.CreateFCmpOLT(left, right, "lt");
	}
}
Value* LLVM_cgen::visit(LECmp * le) {
	Value* left = le->e1->accept(this);
	Value* right = le->e2->accept(this);
	if(le->e1->type == Int) {
		return Builder.CreateICmpSLE(left, right, "le");
	} else if(le->e1->type == Double) {
		return Builder.CreateFCmpOLE(left, right, "le");
	}
}

//Flow of control
Value* LLVM_cgen::visit(ConditionalStmt *p) {
	Value* pred = p->pred->accept(this);
	BasicBlock* ThenBlock = BasicBlock::Create(getGlobalContext(), "then", currentFunction);
	BasicBlock* ElseBlock;
	if(p->false_cond != NULL)
		 ElseBlock = BasicBlock::Create(getGlobalContext(), "else", currentFunction);
	BasicBlock* MergeBlock = BasicBlock::Create(getGlobalContext(), "endif", currentFunction);

	Builder.CreateCondBr(pred, ThenBlock, (p->false_cond != NULL) ? ElseBlock : MergeBlock);
	Builder.SetInsertPoint(ThenBlock);
	Value* ThenV = p->body->accept(this);
	Builder.CreateBr(MergeBlock);

	if(p->false_cond != NULL) {
		Builder.SetInsertPoint(ElseBlock);
		Value *ElseV = p->false_cond->accept(this);
		Builder.CreateBr(MergeBlock);
	}

	Builder.SetInsertPoint(MergeBlock);
	return MergeBlock;
}

Value* LLVM_cgen::visit(WhileLoopStmt *p) {
	BasicBlock* Loop = BasicBlock::Create(getGlobalContext(), "loop", currentFunction);
	BasicBlock* EndLoop = BasicBlock::Create(getGlobalContext(), "endloop", currentFunction);
	
	Value* pred = p->pred->accept(this);
	Builder.CreateCondBr(pred, Loop, EndLoop);

	Builder.SetInsertPoint(Loop);
	p->body->accept(this);
	pred = p->pred->accept(this);
	Builder.CreateCondBr(pred, Loop, EndLoop);

	Builder.SetInsertPoint(EndLoop);
	return EndLoop;
}

///TO BE IMPLEMENTED IN FUTRE VERSIONS           
Value* LLVM_cgen::visit(MatrixPow * d) {
	return ErrorV("POW Not yet supported in this version");
}

Value* LLVM_cgen::visit(Pow * d) {
	return ErrorV("POW Not yet supported in this version");
}