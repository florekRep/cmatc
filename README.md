# Cmat compiler #

Compiler for custom language cmat, based on C with matrix support, backend is LLVM (using C++ LLVM API)

### Build ###

To build this project you will need to have LLVM installed on your machine
Project developed under VisualStudio 2012

### Usage ###

```
#!bash
cmatc <input_file> [-O | -d]
   -O optimizes code using LLVM passes
   -d dumps LLVM bitcode before execution
```

